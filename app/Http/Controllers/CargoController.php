<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cargo;
class CargoController extends Controller
{
  //mostrar todas las cargos
  public function index(Request $request){
      //validacion de seguridad
      if(!$request->ajax()) return redirect('/');
      //$cargo=Especialidad::all(); //devulve todas las categorias
      $buscar=$request->buscar;
      $criterio=$request->criterio;

      if($buscar==''){
        $cargo=Cargo::orderBy('id','desc')->paginate(5);
      }
      else{
        $cargo=Cargo::where($criterio,'like','%'.$buscar.'%')->orderBy('id','desc')->paginate(5);
      }
      return [
        'pagination'=>[
            'total'          => $cargo->total(),
            'current_page'   => $cargo->currentPage(),
            'per_page'       => $cargo->perPage(),
            'last_page'      => $cargo->lastPage(),
            'from'           => $cargo->firstItem(),
            'to'             => $cargo->lastItem(),
        ],
        'cargos'=> $cargo
      ];
  }
  // se rescatan todos los valores del formulario y se guardan en la bd para registrar una nueva especialidad
  public function store(Request $request){
      if(!$request->ajax())
      return redirect('/');
      $cargo= new Cargo();
      $cargo->cargo=$request->cargo;
      $cargo->descripcion=$request->descripcion;
      $cargo->condicion='1';
      $cargo->save();
  }
  //se actualisan los campos del formulario para las especialidades
  public function update(Request $request){
      if(!$request->ajax())
      return redirect('/');
      $cargo=Cargo::findOrFail($request->id);
      $cargo->cargo=$request->cargo;
      $cargo->descripcion=$request->descripcion;
      $cargo->condicion='1';
      $cargo->save();
  }

  //desactiva la condicion//
  public function desactivar(Request $request){
    if(!$request->ajax())
    return redirect('/');
    $cargo=Cargo::findOrFail($request->id);
    $cargo->condicion='0';
    $cargo->save();
  }

  //activa la condicion//
  public function activar(Request $request){
    if(!$request->ajax())
    return redirect('/');
    $cargo=Cargo::findOrFail($request->id);
    $cargo->condicion='1';
    $cargo->save();
  }

}
