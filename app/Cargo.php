<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
  protected $table = 'cargos';
  protected $primaryKey = 'id';
  protected $filleable = ['cargo','descripcion','condicion'];
}
