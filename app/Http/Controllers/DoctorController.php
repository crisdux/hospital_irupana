<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Doctor;
class DoctorController extends Controller{

  //mostrar todas las especialidades
  public function index(Request $request){
      //validacion de seguridad
      //if(!$request->ajax())
      //return redirect('/');
      //$especialidad=Especialidad::all(); //devulve todas las categorias
      $buscar=$request->buscar;
      $criterio=$request->criterio;

      if($buscar==''){
        $doctor=Doctor::join('especialidades','doctores.id_especialidad','=','especialidades.id')
        ->select('doctores.id','doctores.id_especialidad','doctores.p_nombre','doctores.p_apellido','doctores.email','doctores.condicion',
                'especialidades.especialidad','especialidades.descripcion')
        ->orderBy('doctores.id','desc')->paginate(5);
      }
      else{
        $doctor=Doctor::join('especialidades','doctores.id_especialidad','=','especialidades.id')
        ->select('doctores.id','doctores.id_especialidad','doctores.p_nombre','doctores.p_apellido','doctores.email','doctores.condicion',
                'especialidades.especialidad','especialidades.descripcion')
        ->where('doctores.'.$criterio,'like','%'.$buscar.'%')
        ->orderBy('doctores.id','desc')->paginate(5);
      }
      return [
        'pagination'=>[
            'total'          => $doctor->total(),
            'current_page'   => $doctor->currentPage(),
            'per_page'       => $doctor->perPage(),
            'last_page'      => $doctor->lastPage(),
            'from'           => $doctor->firstItem(),
            'to'             => $doctor->lastItem(),
        ],
        'doctores'=> $doctor
      //'nombre tabla   => variable'
      ];
  }
  // se rescatan todos los valores del formulario y se guardan en la bd para registrar una nueva especialidad
  public function store(Request $request){
      if(!$request->ajax())
      return redirect('/');
      $doctor= new Doctor();
      $doctor->id_especilidad=$request->id_especilidad;
      $doctor->p_nombre=$request->p_nombre;
      $doctor->p_apellido=$request->p_apellido;
      $doctor->email=$request->email;
      $doctor->condicion='1';
      $doctor->save();
  }
  //se actualisan los campos del formulario para las especialidades
  public function update(Request $request){
      if(!$request->ajax())
      return redirect('/');
      $doctor=Doctor::findOrFail($request->id);
      $doctor->id_especilidad=$request->id_especilidad;
      $doctor->p_nombre=$request->p_nombre;
      $doctor->p_apellido=$request->p_apellido;
      $doctor->email=$request->email;
      $doctor->condicion='1';
      $doctor->save();
  }
  //desactiva la condicion//
  public function desactivar(Request $request){
    if(!$request->ajax())
    return redirect('/');

    $doctor=Doctor::findOrFail($request->id);
    $doctor->condicion='0';
    $doctor->save();
  }
  //activa la condicion//
  public function activar(Request $request){
    if(!$request->ajax())
    return redirect('/');
    $doctor=Doctor::findOrFail($request->id);
    $doctor->condicion='1';
    $doctor->save();
  }

}
