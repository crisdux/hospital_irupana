<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class DoctorSeeder extends Seeder
{
  public function run()
  {
    DB::table('doctores')->insert([
        array(
          'id_especialidad'=>'1',
          'p_nombre'=>'Alex',
          'p_apellido'=>'Aliaga',
          'email'=>"alex@gmial.com",
          'condicion'=>'1'
        ),
        array(
          'id_especialidad'=>'1',
          'p_nombre'=>'Susana',
          'p_apellido'=>'Pereira',
          'email'=>"susana@gmial.com",
          'condicion'=>'1'
        ),
        array(
          'id_especialidad'=>'2',
          'p_nombre'=>'Pedro',
          'p_apellido'=>'Callizaya',
          'email'=>"pedro@gmial.com",
          'condicion'=>'0'
        ),
        array(
          'id_especialidad'=>'2',
          'p_nombre'=>'Miguel',
          'p_apellido'=>'Acevedo',
          'email'=>"miki@gmial.com",
          'condicion'=>'1'
        ),
        array(
          'id_especialidad'=>'3',
          'p_nombre'=>'Alicia',
          'p_apellido'=>'Quiroga',
          'email'=>"alicia@gmial.com",
          'condicion'=>'1'
        )
    ]);
  }
}
