<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EspecialidadSeeder extends Seeder
{

    public function run()
    {
      DB::table('especialidades')->insert([
          array(
            'especialidad'=>'Medicina General',
            'descripcion'=>'Todo lo relacionado con la medicina general',
            'condicion'=> '0'
          ),
          array(
            'especialidad'=>'Ginecologia',
            'descripcion'=>'todo lo relacionado con mujeres',
            'condicion'=> '0'
          ),
          array(
            'especialidad'=>'Cardiologia',
            'descripcion'=>'todo lo relacionado con el corazon',
            'condicion'=> '1'
          ),
          array(
            'especialidad'=>'Urologia',
            'descripcion'=>'',
            'condicion'=> '1'
          )
      ]);
    }
}
