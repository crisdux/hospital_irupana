<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Especialidad extends Model{
  protected $table = 'especialidades';
  protected $primaryKey = 'id';
  protected $filleable = [
    'especialidad',
    'descripcion',
    'condicion'
  ];

  //una especialidad tiene varios doctores 
  public function doctores(){
    return $this->hasMany('App\Doctor');
  }

}
