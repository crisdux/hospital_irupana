<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    protected $table = 'doctores';
    protected $primaryKey = 'id';

    protected $fillable=[
      "id_especialidad",
      "p_nombre",
      "p_apellido",
      "email",
      "condicion"
    ];

    //varios doctores tienen 1 especialidad
    // un doctor tiene 1 especialidad
    public function especialdiad(){
      return $this->belongsTo('App\Especialidad');
    }


}
