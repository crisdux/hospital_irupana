<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    public function run(){
      $this->call([
       EspecialidadSeeder::class,
       CargoSeeder::class,
       DoctorSeeder::class
      ]);
    }
}

/*
php artisan db:seed
php artisan db:seed --class=UsersTableSeeder
php artisan migrate:refresh --seed
*/
