<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_especialidad')->unsigned();
            $table->string('p_nombre');
            $table->string('p_apellido');
            $table->string('email')->nullable();
            $table->boolean('condicion')->default(1);
            $table->timestamps();

            //relacion
            /*
            $table->foreign('id_especialidad')                     //la llave foranea es: id_especilidad (id_especialidad esta en la tabla doctores)
                  ->references('id')->on('especialidades');  */      //que hace referencia al campo id de la tabla especialidades
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctores');
    }
}
