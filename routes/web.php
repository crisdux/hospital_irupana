<?php
//////////////////////////////////////  RUTA DEL ESCRITORIO ///////////////////////////////
Route::get('/', function () {
    return view('contenido.contenido');
});
/////////////////////////////////////  RUTAS DE LAS ESPECIALIDADES   ////////////////////////////////////
//llama a la ruta especialidad y muestra todas las categorias
Route::get('/especialidad', 'EspecialidadController@index');
//registra una nueva categoria
Route::post('/especialidad/registrar', 'EspecialidadController@store');
//acxtualiza el formulario para las especialidades
Route::put('/especialidad/actualizar', 'EspecialidadController@update');
//desactiva la condicion
Route::put('/especialidad/desactivar', 'EspecialidadController@desactivar');
//activa la condicion
Route::put('/especialidad/activar', 'EspecialidadController@activar');
// llama al metodo par el combo de especialidades
Route::get('/especialidad/selectEspecialidad', 'EspecialidadController@selectEspecialidad');


/////////////////////////////////// RUTAS DE LOS CARGOS /////////////////////////////////////////////
//llama a la ruta cargo y muestra todas las categorias
Route::get('/cargo', 'CargoController@index');
//registra una nueva categoria
Route::post('/cargo/registrar', 'CargoController@store');
//acxtualiza el formulario para las especialidades
Route::put('/cargo/actualizar', 'CargoController@update');
//desactiva la condicion
Route::put('/cargo/desactivar', 'CargoController@desactivar');
//activa la condicion
Route::put('/cargo/activar', 'CargoController@activar');

////////////////////////////////// RUTAS DE LOS DOCTORES ////////////////////////////////////////////
//llama a la ruta cargo y muestra todas los doctores
Route::get('/doctor', 'DoctorController@index');
//registra una nueva categoria
Route::post('/doctor/registrar', 'DoctorController@store');
//acxtualiza el formulario para los doctores
Route::put('/doctor/actualizar', 'DoctorController@update');
//desactiva la condicion
Route::put('/doctor/desactivar', 'DoctorController@desactivar');
//activa la condicion
Route::put('/doctor/activar', 'DoctorController@activar');
