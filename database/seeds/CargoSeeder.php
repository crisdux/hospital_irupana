<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class CargoSeeder extends Seeder{

  public function run()
  {
    DB::table('cargos')->insert([
        array(
          'cargo'=>'Administrador General',
          'descripcion'=>'Cargo relacionado con la medicina general',
          'condicion'=> '0'
        ),
        array(
          'cargo'=>'Jefe de Pediatria',
          'descripcion'=>'Cargo relacionado con niños',
          'condicion'=> '0'
        ),
        array(
          'cargo'=>'Jefe de Ginecologia',
          'descripcion'=>'Cargo relacionado con mujeres',
          'condicion'=> '1'
        ),
        array(
          'cargo'=>'Jefe de Urologia',
          'descripcion'=>'',
          'condicion'=> '1'
        )
    ]);
  }
}
