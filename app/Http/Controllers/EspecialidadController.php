<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Especialidad; //importar el modelo


class EspecialidadController extends Controller{
    //mostrar todas las especialidades
    public function index(Request $request){
        //validacion de seguridad
        if(!$request->ajax())
        return redirect('/');
        //$especialidad=Especialidad::all(); //devulve todas las categorias
        $buscar=$request->buscar;
        $criterio=$request->criterio;

        if($buscar==''){
          $especialidad=Especialidad::orderBy('id','desc')->paginate(5);
        }
        else{
          $especialidad=Especialidad::where($criterio,'like','%'.$buscar.'%')->orderBy('id','desc')->paginate(5);
        }
        return [
          'pagination'=>[
              'total'          => $especialidad->total(),
              'current_page'   => $especialidad->currentPage(),
              'per_page'       => $especialidad->perPage(),
              'last_page'      => $especialidad->lastPage(),
              'from'           => $especialidad->firstItem(),
              'to'             => $especialidad->lastItem(),
          ],
          'especialidades'=> $especialidad
        ];
    }
    // se rescatan todos los valores del formulario y se guardan en la bd para registrar una nueva especialidad
    public function store(Request $request){
        if(!$request->ajax())
        return redirect('/');
        $especialidad= new Especialidad();
        $especialidad->especialidad=$request->especialidad;
        $especialidad->descripcion=$request->descripcion;
        $especialidad->condicion='1';
        $especialidad->save();
    }
    //se actualisan los campos del formulario para las especialidades
    public function update(Request $request){
        if(!$request->ajax())
        return redirect('/');
        $especialidad=Especialidad::findOrFail($request->id);
        $especialidad->especialidad=$request->especialidad;
        $especialidad->descripcion=$request->descripcion;
        $especialidad->condicion='1';
        $especialidad->save();
    }
    //desactiva la condicion//
    public function desactivar(Request $request){
      if(!$request->ajax())
      return redirect('/');
      $especialidad=Especialidad::findOrFail($request->id);
      $especialidad->condicion='0';
      $especialidad->save();
    }
    //activa la condicion//
    public function activar(Request $request){
      if(!$request->ajax())
      return redirect('/');
      $especialidad=Especialidad::findOrFail($request->id);
      $especialidad->condicion='1';
      $especialidad->save();
    }

    //combo box con las especialidades activas para los Doctores
    public function selectEspecialidad(Request $request){
      if(!$request->ajax()) return redirect('/');
      $especialidades= Especialidad::where('condicion','=','1')
      ->select('id','especialidad')
      ->orderBy('especialidad','asc')->get();
      return ['especialidades'=>$especialidades];
    }
}
